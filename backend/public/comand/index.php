<?php
header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');

include '../../classes/index.php';

$data = $_GET;

$path = '../../flows/' . $data['state'] .  '/' . $data['comand'] . '.php';
if(file_exists($path)) {
    include $path;
}
