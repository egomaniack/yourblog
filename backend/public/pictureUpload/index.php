<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');

$uploaddir = __DIR__ . '/../../images/';
$picture;
foreach ($_FILES as $file) {
    $picture = $file;
}

$newFileName = md5(time()) . '.' . pathinfo($picture['name'], PATHINFO_EXTENSION);
$uploadfile = $uploaddir . $newFileName;

if (move_uploaded_file($picture['tmp_name'], $uploadfile)) {
    echo  $_SERVER['SERVER_NAME'] . ':' .
        $_SERVER['SERVER_PORT'] . '/backend/images/' . $newFileName;
} else {
    echo "Возможная атака с помощью файловой загрузки!\n";
};