<?php
header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Credentials: true');

include '../../classes/index.php';

function changeFlow($newStateName = "BlogPosts") {
    $path = "../../flows/$newStateName/INIT.php";
    if(file_exists($path)) {
        include $path;
    } else {
        include '../../flows/BlogPosts/INIT.php';
    }
}

if(isset($_GET['stateName'])) {
    $newStateName = $_GET['stateName'];
    changeFlow($newStateName);
}
