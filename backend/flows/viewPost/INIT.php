<?php

if($_GET['data'] != 'null') {
    $id = $_GET['data'];

    $post = [];
    $db = new DataAccess();
    $post = $db->execute("select * from " . MY_DATABASE_NAME . ".posts where id=$id");
    $db->closeConnection();

    $answer = new FlowServerAnswer();
    $answer->setStateName('viewPost');
    $answer->setInitData($post[0]);

    echo json_encode($answer, JSON_UNESCAPED_UNICODE);
} else {
    include MY_CLASSES_ROOT . '../flows/BlogPosts/INIT.php';
}
