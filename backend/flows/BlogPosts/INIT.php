<?php
$posts = [];
$db = new DataAccess();
$posts = $db->execute("select * from " . MY_DATABASE_NAME . ".posts order by last_update desc");
$db->closeConnection();

$answer = new FlowServerAnswer();
$answer->setStateName('BlogPosts');
$answer->setInitData($posts);

echo json_encode($answer, JSON_UNESCAPED_UNICODE);