<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (!defined('MY_CLASSES_ROOT')) {
    define('MY_CLASSES_ROOT', str_replace("\\", "/", dirname(__FILE__) . '/') );

    include_once(MY_CLASSES_ROOT . 'db.php');
    include_once(MY_CLASSES_ROOT . 'FlowServerAnswer.php');
}
if (!defined('MY_DATABASE_NAME')) {
    define('MY_DATABASE_NAME', 'blog');
}