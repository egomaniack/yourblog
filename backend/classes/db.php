<?php

class DataAccess
{
    private $connect;
    private $error;

    public function __construct () {
        $this->connect = new mysqli("blog-mysql", "root", "root", "blog");

        if ($this->connect->connect_errno) {
            $this->error =  "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL .
                "Код ошибки errno: " . $this->connect->errno . PHP_EOL .
                "Текст ошибки error: " . $this->connect->error . PHP_EOL;

            throw new Exception($this->error);
//            header("HTTP/1.0 406 Not Acceptable");
        }
    }

    public function execute (string $query) {

        $this->connect->query("SET CHARACTER SET utf8");
        $this->connect->query('SET COLLATION_CONNECTION="utf8_general_ci"');
        $answer = $this->connect->query($query);
        $result = [];
        if($answer->num_rows != 0) {
            while ($row = $answer->fetch_assoc()) {
                $result[] = $row;
            }

            $answer->close();
        }

        return $result;
    }

    public function closeConnection() {
        mysqli_close($this->connect);
    }
}