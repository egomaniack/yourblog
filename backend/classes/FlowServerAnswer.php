<?php

class FlowServerAnswer
{
    public $stateName;
    public $initData;

    /**
     * @return mixed
     */
    public function getStateName()
    {
        return $this->stateName;
    }

    /**
     * @param mixed $stateName
     */
    public function setStateName($stateName)
    {
        $this->stateName = $stateName;
    }

    /**
     * @return mixed
     */
    public function getInitData()
    {
        return $this->initData;
    }

    /**
     * @param mixed $initDate
     */
    public function setInitData($initDate)
    {
        $this->initData = $initDate;
    }
}