drop DATABASE if EXISTS blog;
create database blog
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

CREATE TABLE blog.posts (
	id INT NOT NULL AUTO_INCREMENT,
	post_title varchar(100) NULL,
	post_body text NULL,
	picture_path text NULL,
	last_update TIMESTAMP DEFAULT now() NOT NULL,
	CONSTRAINT posts_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;