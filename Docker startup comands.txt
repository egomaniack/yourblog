docker rm --force blog-mysql
docker rm --force blog-srv

reset
docker rm --force blog-mysql && docker run --name blog-mysql -e MYSQL_ROOT_PASSWORD=root -p 3308:3306 -d mysql && docker rm --force blog-srv && docker run -d -v c:/code/interview/blog/backend:/app --name blog-srv -p 8080:80 --link blog-mysql:blog-mysql webdevops/php-apache-dev

new
docker run --name blog-mysql -e MYSQL_ROOT_PASSWORD=root -p 3308:3306 -d mysql && docker run -d -v c:/code/interview/blog/backend:/app --name blog-srv -p 8080:80 --link blog-mysql:blog-mysql webdevops/php-apache-dev