const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require("path");
const webpack = require("webpack");
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const defEnv = {
    production: false,
    backEndPort:"8080"
};

module.exports = (env = defEnv) => {
    return {
        entry: {
            app: "./src/index.tsx",
            reactjs: [
                'react', 'react-dom'
            ]
        },
        output: {
            filename: "[name].bundle.js",
            path: path.resolve(__dirname, "dist"),
            publicPath: path.build
        },
        node: {
            fs: "empty"
        },
        plugins: [
            env.production && new UglifyJsPlugin() || (() => {}),
            env.production && new FaviconsWebpackPlugin({
                logo: './src/pics/favicon.svg',
                // The prefix for all image files (might be a folder or a name)
                prefix: 'icons-[hash]/',
                // Emit all stats of the generated icons
                emitStats: false,
                // The name of the json containing all favicon information
                statsFilename: 'iconstats-[hash].json',
                // Generate a cache file with control hashes and
                // don't rebuild the favicons until those hashes change
                persistentCache: true,
                // Inject the html into the html-webpack-plugin
                inject: true,
                // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
                background: '#fff',
                // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
                title: 'Webpack App',

                // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
                icons: {
                android: false,
                appleIcon: false,
                appleStartup: false,
                coast: false,
                favicons: true,
                firefox: false,
                opengraph: false,
                twitter: false,
                yandex: false,
                windows: false
                }
            }) || (() => {}),
            new webpack.EnvironmentPlugin({
                itIsDev: !env.production,
                port: env.backEndPort
            }),
            new HtmlWebpackPlugin({
                title: "Ваш блог",
                minify:{
                    collapseWhitespace: false
                },
                template: './src/index.html',
                hash:true
            })
        ],
        devServer:{
            contentBase: path.join(__dirname,"dist"),
            compress: true,
            stats: "errors-only",
            port: 9002,
            open: true
        },
    
        // Enable source maps for debugging webpack's output.
        devtool: !env.production ? "source-map" : false,

        resolve: {
            extensions: [ ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
        },

        module: {
            loaders: [
                // все файлы с расширением '.ts' or '.tsx' будут обработаны 'awesome-typescript-loader'.
                { test: /\.tsx?$/, use: "awesome-typescript-loader" },
                { test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/, use: "file-loader" }
                ],
    
        },
    }
};