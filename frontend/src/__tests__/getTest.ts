import { getParameterByName } from "../app/helpers/getGetParameterByName";

describe('Проверка получения get параметров', function(){

    const get = "www.example.com/test?testParam=3&flowTestName=mainFlow";

    it ("вытащит testParam из get строки",function(){
        expect(getParameterByName("testParam",get)).toBe("3");
    });

    it ("вытащит flowTestName из get строки",function(){
        expect(getParameterByName("flowTestName",get)).toBe("mainFlow");
    });

    it ("вытащит несуществующий параметр из get строки",function(){
        expect(getParameterByName("none",get)).toBeNull;
    });

});