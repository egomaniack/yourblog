import { createGetParams } from '../app/helpers/createGetParams';

describe('Создаем get строку на основе объекта', function() {

    it('Вернет правильные get параметры', function() {
        expect(createGetParams({aaa: 111, bbb: 222})).toEqual('?aaa=111&bbb=222');
    });
    it('Вернет правильные get параметры', function() {
        expect(createGetParams({aaa: 111})).toEqual('?aaa=111');
    });
    it('Вернет правильные get параметры', function() {
        expect(createGetParams({bbb: 222})).toEqual('?bbb=222');
    });
    it('Вернет пустую строку', function() {
        expect(createGetParams({})).toEqual('');
    });
    it('Вернет правильные get параметры', function() {
        expect(createGetParams({bbb: 222, u: 'testingString'})).toEqual('?bbb=222&u=testingString');
    });
    it('Вернет правильные get параметры', function() {
        expect(createGetParams({fio: 'Фамилия Имя Отчество', u: 'testingString'}))
        // tslint:disable-next-line:max-line-length
        .toEqual('?fio=%D0%A4%D0%B0%D0%BC%D0%B8%D0%BB%D0%B8%D1%8F%20%D0%98%D0%BC%D1%8F%20%D0%9E%D1%82%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%BE&u=testingString');
    });
    it('Вернет правильные get параметры', function() {
        expect(createGetParams({fio: 'Jan jak', testingString: 'testingString'}))
        .toEqual('?fio=Jan%20jak&testingString=testingString');
    });
    it ('null обработается так', function() {
        expect(createGetParams({c: null})).toBe('?c=null');
    });
    it ('null обработается так', function() {
        const a = undefined;
        expect(createGetParams(a)).toBe('');
    });
});