import * as Redux from 'redux';
import {connect} from 'react-redux';

import reducers from './reducers';
import * as styles from './styled';
import * as MyActionTypes from './constants/Constants';
import Application from './Controller';
import { Command, changeState } from "./helpers/fetching";

const mapStateToProps = (state: any) => ({
  state,
  stateName: state.flow.stateName,
  error: state.error,
  loading: state.flow.fetching
});

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>, getState: any) => {
  return {
    changeState: Redux.bindActionCreators(changeState, dispatch),
  };
}
let connectedApplicatin = connect(mapStateToProps, mapDispatchToProps)(Application);
(connectedApplicatin as any).reducer = reducers;

export default connectedApplicatin;