import * as React from 'react';
import { Button } from 'material-ui';

import { PictureUpload } from '../../components/PictureUpload';
import { dictionary } from '../../constants/dictionary';
import { 
  Card,
  PostBody,
  TitleInput,
  PostTitle,
} from './styled';
import { Post } from '../../interfaces/post';

interface AddPostProps {
  saveEditedPost(post: any): Promise<any>;
  changeState(stateName: string);
  post: Post;
}

interface AddPostState {
  pickedPictureUrl?: string;
}
class editPostView extends React.PureComponent <AddPostProps, AddPostState> {

  private fileReader: FileReader;
  private titleInput: HTMLInputElement;
  private bodyInput: HTMLTextAreaElement;

  constructor(props: AddPostProps) {
    super(props);
    
    this.fileReader = new FileReader();
    this.state = {
      pickedPictureUrl: props.post.picture_path
    }

    this.fileReader.onload = ({target}: any) => {
      this.setState({pickedPictureUrl: target.result});
    }
  }

  savePost = () => {
    let post = {
      id: this.props.post.id,
      post_title: this.titleInput.value,
      post_body: this.bodyInput.value,
      picture_path: this.state.pickedPictureUrl
    };
  }

  render() {
    let { pickedPictureUrl } = this.state;
    let { post } = this.props;
    return (
      <Card gotPicture={!!pickedPictureUrl}>
        {this.state.pickedPictureUrl && <img src={pickedPictureUrl}/>
      || <PictureUpload onUpload={(pictureUrl: string) => this.setState({pickedPictureUrl: pictureUrl})}/>}
        <PostTitle><p>{post.post_title}</p></PostTitle>
        <PostBody>{post.post_body}</PostBody>
      </Card>
    );
  }
};

export default editPostView;