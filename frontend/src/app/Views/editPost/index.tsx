import * as React from 'react';
import { Button } from 'material-ui';

import { PictureUpload } from '../../components/PictureUpload';
import { dictionary } from '../../constants/dictionary';
import { 
  Card,
  PostBody,
  TitleInput
} from './styled';
import { Post } from '../../interfaces/post';

interface AddPostProps {
  saveEditedPost(post: any): Promise<any>;
  changeState(stateName: string);
  post: Post;
}

interface AddPostState {
  pickedPictureUrl?: string;
}
class editPostView extends React.PureComponent <AddPostProps, AddPostState> {

  private fileReader: FileReader;
  private titleInput: HTMLInputElement;
  private bodyInput: HTMLTextAreaElement;

  constructor(props: AddPostProps) {
    super(props);
    
    this.fileReader = new FileReader();
    this.state = {
      pickedPictureUrl: props.post.picture_path
    }

    this.fileReader.onload = ({target}: any) => {
      this.setState({pickedPictureUrl: target.result});
    }
  }

  savePost = () => {
    let post = {
      id: this.props.post.id,
      post_title: this.titleInput.value,
      post_body: this.bodyInput.value,
      picture_path: this.state.pickedPictureUrl
    };

    this.props.saveEditedPost(post)
    .then(() => this.props.changeState('BlogPosts'));
    
  }

  render() {
    let { pickedPictureUrl } = this.state;
    let { post } = this.props;
    return (
      <Card gotPicture={!!pickedPictureUrl}>
        {this.state.pickedPictureUrl && <img src={pickedPictureUrl}/>
      || <PictureUpload onUpload={(pictureUrl: string) => this.setState({pickedPictureUrl: pictureUrl})}/>}
        <TitleInput defaultValue={post.post_title} innerRef={input => this.titleInput = input} placeholder={dictionary.ADDTITLE} />
        <PostBody defaultValue={post.post_body} innerRef={textarea => this.bodyInput = textarea} placeholder={dictionary.ADDBODY} />
        <Button onClick={this.savePost} color="primary" variant="raised">{dictionary.add}</Button>
      </Card>
    );
  }
};

export default editPostView;