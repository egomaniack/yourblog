import styled from 'styled-components';

export const Wrapper = styled.main`
  padding-top: 20px;
  display: grid;
  grid-template-columns: 0 100% 0;
  grid-row-gap: 25px;

  @media (min-width: 768px) {
    grid-template-columns: auto 80vw auto;
  }

  section {
    grid-column: 2;
  }
`;