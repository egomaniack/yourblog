import * as React from 'react';

import { Post } from 'app/interfaces/post';
import { PostCard } from '../../components/postCard';
import {
    Wrapper
} from './styled';

interface BlogPostsProps {
    posts: Array<Post>;
    editPost(postId: number): void;
    viewPost(postId: number): void;
}

const BlogPostsView = (props: BlogPostsProps) => {
    let { posts } = props;

    return (
        <Wrapper>
            {posts.map((post: Post) => <PostCard
                editPost={props.editPost}
                key={post.id}
                post={post}
                viewPost={props.viewPost}
            />)}
        </Wrapper>
    );
};

export default BlogPostsView;