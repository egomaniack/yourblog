import styled from 'styled-components';

import { Colors } from '../../constants/colors';
import { FONTS_STACK } from '../../constants/Constants';

interface GotPictureProp {
  gotPicture: boolean;
}


export const Card = styled.div`
  padding-top: 20px;
  width: 80vw;
  margin: 0 auto;
  background-color: white;
  display: grid;
  grid-template-rows: ${(props: GotPictureProp) => props.gotPicture ? '350px' : '60px'} 70px 300px 60px;

  & > img {
    height: 100%;
    margin: 0 auto;
    width: auto;
    max-width: 100%;
  }

  & > button {
    width: 150px;
    height: 35px;
    align-self: center;
    margin: 0 auto;
  }
`;

export const TitleInput = styled.input `
  padding: 10px;
  margin: 15px;
  border: 1px solid ${Colors.softGray};
  font-size: 17px;
  font-family: ${FONTS_STACK.ROBOTO};
  font-style: italic;
`;

export const PostBody = styled.textarea`
  display: block;
  padding: 15px;
  outline: none;
  text-indent: 15px;
  margin: 15px;
  border: 1px solid ${Colors.softGray};
  resize: none;
`;