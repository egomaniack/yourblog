import * as React from 'react';
import { Button } from 'material-ui';

import { PictureUpload } from '../../components/PictureUpload';
import { dictionary } from '../../constants/dictionary';
import { 
  Card,
  PostBody,
  TitleInput
} from './styled';

interface AddPostProps {
  saveNewPost(post: any): Promise<any>;
  changeState(stateName: string);
}

interface AddPostState {
  pickedPictureUrl?: string;
}
class addPostView extends React.PureComponent <AddPostProps, AddPostState> {

  private fileReader: FileReader;
  private titleInput: HTMLInputElement;
  private bodyInput: HTMLTextAreaElement;

  constructor(props: AddPostProps) {
    super(props);
    
    this.fileReader = new FileReader();
    this.state = {
      pickedPictureUrl: null
    }

    this.fileReader.onload = ({target}: any) => {
      this.setState({pickedPictureUrl: target.result});
    }
  }

  savePost = () => {
    let post = {
      post_title: this.titleInput.value,
      post_body: this.bodyInput.value,
      picture_path: this.state.pickedPictureUrl
    };

    this.props.saveNewPost(post)
    .then(() => this.props.changeState('BlogPosts'));
    
  }

  render() {
    let { pickedPictureUrl } = this.state;
    return (
      <Card gotPicture={!!pickedPictureUrl}>
        {this.state.pickedPictureUrl && <img src={pickedPictureUrl}/>
      || <PictureUpload onUpload={(pictureUrl: string) => this.setState({pickedPictureUrl: pictureUrl})}/>}
        <TitleInput innerRef={input => this.titleInput = input} placeholder={dictionary.ADDTITLE} />
        <PostBody innerRef={textarea => this.bodyInput = textarea} placeholder={dictionary.ADDBODY} />
        <Button onClick={this.savePost} color="primary" variant="raised">{dictionary.add}</Button>
      </Card>
    );
  }
};

export default addPostView;