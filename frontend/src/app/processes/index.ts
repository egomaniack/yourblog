import BlogPosts from '../containers/Blogosts';
import addPost from '../containers/addPosts';
import editPost from '../containers/editPost';
import viewPost from '../containers/viewPost';

export default {
  BlogPosts,
  addPost,
  editPost,
  viewPost
}