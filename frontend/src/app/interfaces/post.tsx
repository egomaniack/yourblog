export interface Post {
  id: number;
  post_title: string;
  post_body: string;
  picture_path?: string;
  last_update: Date;
}