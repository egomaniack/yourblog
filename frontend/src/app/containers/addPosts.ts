import * as React from 'react';
import * as Redux from 'redux';
import {connect} from 'react-redux';

import AddPostView from '../Views/addPost/';
import { executeCommand, Command }from '../helpers/fetching';
import { Post } from '../interfaces/post';


const mapStateToProps = (state: any) => ({
});

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>) => ({
    saveNewPost: (post: Post) => executeCommand(Command.NEW, { state: 'addPost' }, { post }, true)(dispatch)
});

let connectedApp = connect(mapStateToProps, mapDispatchToProps)(AddPostView as any);
export default connectedApp;