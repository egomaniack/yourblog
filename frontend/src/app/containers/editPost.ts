import * as React from 'react';
import * as Redux from 'redux';
import {connect} from 'react-redux';

import EditPost from '../Views/editPost/';
import { executeCommand, Command }from '../helpers/fetching';
import { Post } from '../interfaces/post';

const mapStateToProps = (state: any) => ({
    error: state.error.message,
    flow: state.flow,
    post: state.flow.initData
});

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>) => ({
  saveEditedPost: (post: Post) => executeCommand(Command.SAVE, { state: 'editPost' }, { post }, true)(dispatch)
});

let connectedApp = connect(mapStateToProps, mapDispatchToProps)(EditPost as any);
export default connectedApp;