import * as React from 'react';
import * as Redux from 'redux';
import {connect} from 'react-redux';

import { changeState } from '../helpers/fetching';
import BlogPostsView from '../Views/BlogPosts/';

import { Post } from '../interfaces/post';


const mapStateToProps = (state: any) => ({
    error: state.error.message,
    flow: state.flow,
    posts: state.flow.initData || []
});

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>) => ({
    editPost: (postId: number) => changeState('editPost', postId)(dispatch),
    viewPost: (postId: number) => changeState('viewPost', postId)(dispatch),
});

let connectedApp = connect(mapStateToProps, mapDispatchToProps)(BlogPostsView as any);
export default connectedApp;