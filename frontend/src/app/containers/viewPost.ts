import * as React from 'react';
import * as Redux from 'redux';
import {connect} from 'react-redux';

import ViewPost from '../Views/viewPost/';
import { executeCommand, Command }from '../helpers/fetching';
import { Post } from '../interfaces/post';

const mapStateToProps = (state: any) => ({
    error: state.error.message,
    flow: state.flow,
    post: state.flow.initData
});

const mapDispatchToProps = (dispatch: Redux.Dispatch<any>) => ({
});

let connectedApp = connect(mapStateToProps, mapDispatchToProps)(ViewPost as any);
export default connectedApp;