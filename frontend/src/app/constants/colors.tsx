export const Colors = Object.freeze({
    white: '#FAFAFA',
    gray: '#D3D3D3',
    mainColor: '#2ea2f8',
    mainHovered: '#54B1F8',
    mainClicked: '#2B95E7',
    notActiveColor: 'gray',
    orangeColor: '#ff7800',
    orangeColorHovered: '#FFA353',
    error: 'red',
    softGray: '#ccc',
    accent: '#8a2be2',

    primory: '#2ea2f8',
    pLight: "#75d3ff",
    pDark: "#0074c5",
    secondary: "#ff7800",
    sLight: "#ffa942",
    sDark: "#c54800",
});

export const RgbColors = Object.freeze({
    green: '107,149,129',
    infoGreen: "107,149,129",
    infoPurp: "112,110,168",
});

export const AvatarBackgrounds = [
    '#56CAF6',
    '#00FFA3',
    '#F26447',
    '#09DFF2'
];