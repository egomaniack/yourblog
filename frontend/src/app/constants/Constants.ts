export const CHANGE_STATE = 'CHANGE_STATE';
export const REQUEST_STATE = 'REQUEST_STATE';
export const ERROR = 'ERROR';
export const SUCCESS = 'SUCCESS';
export const FETCHING = 'FETCHING';
export const UPDATE_ERROR_MESSAGE = 'UPDATE_ERROR_MESSAGE';

export const FONTS_STACK = Object.freeze({
    ROBOTO: 'Roboto, sans-serif',
    RUSSOONE: 'RussoOne'
});