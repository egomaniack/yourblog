
const port = (process.env.NODE_ENV === 'test') &&
process.env.npm_package_jest_globals_PORT ||
process.env.port;

export const serverAddr = `localhost:${port}/backend/public`;

export const dictionary = Object.freeze({
    SEND: 'Отправить',
    SEARCHING: 'Поиск...',
    LOADING: "Загрузка...",
    UPDATED: 'обновлено',
    ADDTITLE: 'Введите заголовок статьи',
    ADDBODY: 'Писать тут)',
    DROPHERE: 'Перетащите картинку сюда или нажмите для выбора файла',

    nickName: 'Придумайте никнэйм',

    //Кнопки
    addPost: '+ новый пост',
    add: 'Сохранить',
    next: 'Далее',
    prew: 'Назад',
    change: 'Изменить',
    addPic: 'картинка ',
});

export const PICTURE_PATH = `http://${serverAddr}/images/`;