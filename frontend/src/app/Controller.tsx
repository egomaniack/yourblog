import * as React from 'react';

import { Header } from './components/header';
import processes from './processes';
import { getParameterByName } from './helpers/getGetParameterByName';


class Application extends React.Component < any, {} > {

  componentDidMount() {
    this.initState();/**запускаем нужный State из GET если таковой задан и не указывает на главную страницу */
    window.onpopstate = () => this.initState(false);
  };
  
  private initState = (pushHistory: boolean = true) => {
    let UrlStateName = getParameterByName('stateName');
    if (!UrlStateName)
      UrlStateName = "BlogPosts";
    this.props.changeState(UrlStateName, null, false);
  }

  render(){
    let {stateName} = this.props;
    let Component = (processes as any)[stateName];

    return (<>
        <Header
          toMain={() => this.props.changeState('BlogPosts')}
          addPost={() => this.props.changeState('addPost')}
          loading={this.props.loading}
          stateName={this.props.stateName}
        />
        <Component {...this.props} />
        </>
    );
  };
}
export default Application;