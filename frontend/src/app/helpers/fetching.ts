import * as Redux from 'redux';

import { serverAddr } from "../constants/dictionary";
import { createGetParams } from './createGetParams';
import * as MyActionTypes from '../constants/Constants';

export enum Command {
  NEW = 'NEW',
  SAVE = 'SAVE',
}
interface Params {
  state: string;
}
export const executeCommand = (comand: Command ,params: Params , data: object = {}, silence = false) => (dispatch: any) => new Promise((resolve)=>{
  dispatch({ type: MyActionTypes.REQUEST_STATE });
  let { state } = params;
  let formData = new FormData();

  formData.append('json', JSON.stringify(data));

  const fetchParams: RequestInit = {
    method: 'POST',
    body: formData,
    credentials: 'include'
  };

  fetch(`http://${serverAddr}/comand/${createGetParams({comand, state})}`,
    fetchParams)
    .then(function(response: any) {
      return silence || response.json();
    })
    .then(function(result: any) {
        let { flow } = result;
        silence || window.history.pushState(
          { html: "index.html", pageTitle: "Ваш блог" }, "", createGetParams({stateName: flow.stateName})
        );
      silence || dispatch({ type: MyActionTypes.CHANGE_STATE, payload: { data: result } });
      resolve();
  });
});

export const changeState = ( stateName: string, data?: any, pushHistory: boolean = true ) => (dispatch: Redux.Dispatch<any>) => {
  dispatch({ type: MyActionTypes.REQUEST_STATE });
  const getparams = createGetParams({stateName, data});
  const fetchParams = {
    method: 'GET',
  };
  fetch(`http://${serverAddr}/changeFlow/${getparams}`, fetchParams)
  .then(function(responce: any) { return responce.json()})
  .then(function(result: any) {
        let { stateName, initData } = result;
        if(pushHistory) {
          window.history.pushState(
            { html: "index.html", pageTitle: "Ваш блог" }, "", createGetParams({stateName})
          );
        }
        dispatch({ type: MyActionTypes.CHANGE_STATE, payload: { ...result } });
      })
      .catch(error =>{
        console.info('error', error);
        dispatch({ type: MyActionTypes.ERROR, payload: { error } })
      }
      );
};