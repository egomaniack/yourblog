export const getParameterByName = (name: string, url: string = window.location.href): string => {
    /**
     * парсит GET строку. Возвращает запрошенный параметр
     */
    name = name.replace(/[\[\]]/g, "\\$&");
    let results = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`).exec(url);

    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}