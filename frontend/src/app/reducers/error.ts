import * as actionTypes from '../constants/Constants'

interface ErrorState {
  message: string;
}

interface ErrorAction {
  type: string;
  payload: ErrorPayload;
}

interface ErrorPayload {
  message: string;
}

const initialState: ErrorState = {
    message: null
};

export const error = (state: ErrorState = initialState, action: ErrorAction) => {
  switch (action.type) {
    case actionTypes.ERROR:
      return {
          ...state,
          message: "Проблемы при соединении с сервером"
        }
    default:
      return state;
  }
};