import * as actionTypes from '../constants/Constants';

interface FlowState {
  stateName: string;
  initData: any;
  errorMessage: string;
};

interface Flowayload {
  stateName: string;
  initData?: any;
};

interface FlowAction {
  type: string;
  payload: Flowayload | null;
};

const initialState: FlowState = {
  stateName: "BlogPosts",
  initData: null,
  errorMessage: ''
};

export const flow = (state: FlowState = initialState, action: FlowAction) => {
  switch (action.type) {
    case actionTypes.CHANGE_STATE:
      const { stateName, initData } =  action.payload;
      return {
        ...state,
        stateName,
        initData,
        fetching: false
      }
    case actionTypes.REQUEST_STATE:
      return {
        ...state, fetching: true
      }
    default:
      return state;
  }
};