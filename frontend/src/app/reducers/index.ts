import {combineReducers} from 'redux';

import {flow} from './Flow';
import {error} from './error';

export default combineReducers({
  error,
  flow,
});