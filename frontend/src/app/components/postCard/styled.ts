import styled from 'styled-components';
import { FONTS_STACK } from '../../constants/Constants';

import { dictionary } from '../../constants/dictionary';
import { Colors } from '../../constants/colors';

interface GotPictureProp {
  gotPicture: boolean;
}

export const PostWrapper = styled.section`
  cursor: pointer;
  position: relative;
  background-color: white;
  box-shadow: 0 0 12px rgba(0, 0, 0, 0.2);
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: ${(props: GotPictureProp) => props.gotPicture ? '350px' : ''} 70px 250px 15px;

  & > img {
    height: auto;
    max-height: 100%;
    max-width: 100%;
    width: auto;
    margin: 0 auto;
  }

  & > svg {
    position: absolute;
    right: 5px;
    opacity: 0.45;
    cursor: pointer;
  }

  @media (min-width: 768px) {
    & > svg {
      right: -26px;
    }
  }

  &:hover > svg {
    opacity: 1;
  }
`;

export const PostTitle = styled.div`
  font-family: ${FONTS_STACK.ROBOTO};
  font-style: italic;
  font-size: 20px;
  text-align: center;
  align-self: center;
`;

export const PostBody = styled.textarea`
  display: block;
  padding: 15px;
  outline: none;
  text-indent: 15px;
  border: none;
  resize: none;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const LastEdit = styled.div`
  text-align: right;
  align-self: center;
  color: ${Colors.softGray};

  &::before {
    content: '${`${dictionary.UPDATED}: `}';
  }
`;