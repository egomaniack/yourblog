import * as React from 'react';
import { Button } from 'material-ui';
import { Edit } from 'material-ui-icons';

import { PostWrapper, PostTitle, PostBody, LastEdit } from './styled';
import { Post } from '../../interfaces/post';

interface PostProps {
  post: Post;
  editPost(postId: number): void;
  viewPost(postId: number): void;
}

export const PostCard = (props: PostProps) => {
  let { post } = props;
  let title = post.post_title;
  let body = post.post_body;
  let lastUpdate = new Date(post.last_update);

  let handleWrapperClick = (e) => {
    if(e.target.id !== 'editButton') {
      props.viewPost(props.post.id);
    }
  };

  return (
    <PostWrapper onClick={handleWrapperClick} gotPicture={!!post.picture_path}>
      <Edit id="editButton" onClick={() => props.editPost(post.id)} />
      {!!post.picture_path && <img src={post.picture_path}/>}
      <PostTitle>{title}</PostTitle>
      <PostBody readOnly={true} value={body}/>
      <LastEdit>{lastUpdate.toLocaleDateString()}</LastEdit>
    </PostWrapper>
  );
};
