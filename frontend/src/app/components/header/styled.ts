import styled from 'styled-components';

import { Colors } from '../../constants/colors'

export const HeaderWrap = styled.header`
  background-color: ${ Colors.pLight };
  width: 100%;
  height: 70px;
  display: grid;
  grid-template-columns: 100px auto 180px 10px;
  grid-template-rows: 55px 10px;

  & > button {
    grid-column: 3;
    height: 40px;
    align-self: flex-end;
  }

  & > div {
    grid-column: 1 / 5;
    grid-row: 3;
    align-self: flex-end;
  }
`;

export const Logo = styled.img`
  cursor: pointer;
  height: 50px;
  padding: 10px 10px 0;
`;