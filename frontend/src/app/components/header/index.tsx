import * as React from 'react';
import { Button } from 'material-ui';

import { dictionary } from '../../constants/dictionary';
import { LinearProgress } from 'material-ui';
import {
  HeaderWrap,
  Logo
} from './styled';

interface HeaderProps {
  stateName: string;
  loading: boolean;
  addPost(): void;
  toMain(): void;
}

const logo = require('../../../pics/Logo.png');

export const Header = (props: HeaderProps) => {

  return (
    <HeaderWrap>
      <Logo
        src={logo}
        onClick={props.toMain}
      />
      {props.loading && <LinearProgress />}
      {props.stateName != 'addPost' &&
      <Button
        variant="raised"
        color="primary"
        onClick={props.addPost}
      >
        {dictionary.addPost}
      </Button>
      }
    </HeaderWrap>
  );
};