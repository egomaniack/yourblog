import * as React from 'react';
import { Button } from 'material-ui';
import FileUpload from 'material-ui-icons/FileUpload';
// import Dropzone from 'react-dropzone';
let request = require('superagent');

import { Drop } from './styled';
import { dictionary, serverAddr } from '../../constants/dictionary';

interface PictureUploadProps {
    onUpload(url: string): void;
}

export class PictureUpload extends React.PureComponent <PictureUploadProps, any> {
    private formRef: HTMLFormElement;

    constructor(props: PictureUploadProps) {
        super(props);
        this.state = {
            filesToBeSent: null
        }
    }

    pictureInputChange = ({target}: any) => {
        if(target.files && target.files[0]) {
            this.formRef.submit();
        }
    }

    onDrop = (acceptedFiles) => {
        this.setState({filesToBeSent: acceptedFiles}, this.handleSendFile); 
    }

    handleSendFile = () => {
        if(this.state.filesToBeSent){
            let filesArray = this.state.filesToBeSent;
            let req = request.post(`http://${serverAddr}/pictureUpload/`);
            req.attach(filesArray[0].name, filesArray[0]);
            req.end((err,res) => {
            if(err){
                console.error("error ocurred");
            }
            this.props.onUpload('http://' + res.text);
            });
        }
    }

    render() {

        return (
            <Drop multiple={false} onDrop={(files) => this.onDrop(files)}>
                <div>{dictionary.DROPHERE}</div>
            </Drop>
        );
    }
};