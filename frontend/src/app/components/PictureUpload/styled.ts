import styled from 'styled-components';
import Dropzone from 'react-dropzone';

import { Colors } from '../../constants/colors';

export const Drop = styled(Dropzone)`
  width: 70%;
  margin: 0 auto;
  border-width: 2px;
  border-color: ${Colors.mainColor};
  border-style: dashed;
  border-radius: 5px;

  div {
    text-align: center;
  }
`;